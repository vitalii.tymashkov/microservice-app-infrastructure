#!/bin/bash

### Git
sudo yum install git

git clone https://github.com/Vitaliitymashkov/microservice-app-example.git
cd microservice-app-example/

### Docker and docker compose prerequisites
#sudo apt-get install curl
#sudo apt-get install gnupg
#sudo apt-get install ca-certificates
#sudo apt-get install lsb-release
#
#sudo mkdir -p /etc/apt/keyrings
#curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg

### Docker2
#sudo snap install docker -y
sudo apt  install docker.io -y
sudo chmod 777 /var/run/docker.sock

### Docker-compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
