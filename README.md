# microservice-app-infrastructure

## Getting started

This project contains infrastructure for _microservice-app-example_
https://github.com/elgris/microservice-app-example

### Documentation is here
https://docs.google.com/document/d/1Ex1rm4p7Y3V4OvSd0DDSvkGNQ_pDIytbTUlLocS024g/edit#

## Steps

1. Create Infra repo in Terraform code

AWS

      EC2 instance
      Security group


Hints on terraform

    terraform init +++передача тфстейта!!!
    terraform validate
    terraform plan -var "env=dev" -out dev_infra.bin
    terraform apply dev_infra.bin
    terraform apply dev_infra.bin -auto-approve


    terraform destroy


Connect to EC2

    ssh -i "app-msa-dev.pem" ec2-user@ec2-54-167-163-154.compute-1.amazonaws.com


+1 
Execute the following commands in git bash:

    terraform init
    terraform plan
    terraform apply -auto-approve

    terraform destroy -auto-approve

https://ec2-184-73-19-174.compute-1.amazonaws.com:8080/hello

54.208.119.213
https://ec2-54-208-119-213.compute-1.amazonaws.com/
ssh -i "app-msa-dev.pem" ec2-user@ec2-54-208-119-213.compute-1.amazonaws.com

curl 172.31.86.253:8080/hello


++++++++++++++++++
git clone https://github.com/Vitaliitymashkov/microservice-app-example.git
cd microservice-app-example/
docker-compose up -d


+----
ubuntu@ip-172-31-89-54:~$ git clone https://github.com/Vitaliitymashkov/microservice-app-example.git
Cloning into 'microservice-app-example'...
remote: Enumerating objects: 699, done.
remote: Counting objects: 100% (699/699), done.
remote: Compressing objects: 100% (314/314), done.
remote: Total 699 (delta 328), reused 699 (delta 328), pack-reused 0
Receiving objects: 100% (699/699), 2.64 MiB | 18.03 MiB/s, done.
Resolving deltas: 100% (328/328), done.
ubuntu@ip-172-31-89-54:~$ cd microservice-app-example/
ubuntu@ip-172-31-89-54:~/microservice-app-example$ docker-compose up -d
Traceback (most recent call last):
File "urllib3/connectionpool.py", line 677, in urlopen
File "urllib3/connectionpool.py", line 392, in _make_request
File "http/client.py", line 1277, in request
File "http/client.py", line 1323, in _send_request
File "http/client.py", line 1272, in endheaders
File "http/client.py", line 1032, in _send_output
File "http/client.py", line 972, in send
File "docker/transport/unixconn.py", line 43, in connect
PermissionError: [Errno 13] Permission denied

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
File "requests/adapters.py", line 449, in send
File "urllib3/connectionpool.py", line 727, in urlopen
File "urllib3/util/retry.py", line 410, in increment
File "urllib3/packages/six.py", line 734, in reraise
File "urllib3/connectionpool.py", line 677, in urlopen
File "urllib3/connectionpool.py", line 392, in _make_request
File "http/client.py", line 1277, in request
File "http/client.py", line 1323, in _send_request
File "http/client.py", line 1272, in endheaders
File "http/client.py", line 1032, in _send_output
File "http/client.py", line 972, in send
File "docker/transport/unixconn.py", line 43, in connect
urllib3.exceptions.ProtocolError: ('Connection aborted.', PermissionError(13, 'Permission denied'))

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
File "docker/api/client.py", line 214, in _retrieve_server_version
File "docker/api/daemon.py", line 181, in version
File "docker/utils/decorators.py", line 46, in inner
File "docker/api/client.py", line 237, in _get
File "requests/sessions.py", line 543, in get
File "requests/sessions.py", line 530, in request
File "requests/sessions.py", line 643, in send
File "requests/adapters.py", line 498, in send
requests.exceptions.ConnectionError: ('Connection aborted.', PermissionError(13, 'Permission denied'))

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
File "docker-compose", line 3, in <module>
File "compose/cli/main.py", line 81, in main
File "compose/cli/main.py", line 200, in perform_command
File "compose/cli/command.py", line 70, in project_from_options
File "compose/cli/command.py", line 153, in get_project
File "compose/cli/docker_client.py", line 43, in get_client
File "compose/cli/docker_client.py", line 170, in docker_client
File "docker/api/client.py", line 197, in __init__
File "docker/api/client.py", line 222, in _retrieve_server_version
docker.errors.DockerException: Error while fetching server API version: ('Connection aborted.', PermissionError(13, 'Permission denied'))
[5715] Failed to execute script docker-compose



git clone https://github.com/Vitaliitymashkov/microservice-app-example.git
cd microservice-app-example

docker-compose up -d

curl -v https://google.com
curl http://localhost:8080/


sudo ss -tulpn | grep LISTEN


# 20230226
ami           = "ami-0dfcb1ef8550277af"
instance_type = "t2.large"

sudo yum install git