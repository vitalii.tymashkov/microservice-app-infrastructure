provider "aws" {
  region = "us-east-1"
  shared_credentials_files = [".aws/credentials"]
}

#resources
resource "aws_vpc" "vpc" {
  cidr_block = var.cidr_vpc
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = {
    "Environment" = var.environment_tag
  }
}

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id
  tags = {
    "Environment" = var.environment_tag
  }
}

resource "aws_subnet" "subnet_public" {
  vpc_id = aws_vpc.vpc.id
  cidr_block = var.cidr_subnet
  map_public_ip_on_launch = "true"
  availability_zone = var.availability_zone
  tags = {
    "Environment" = var.environment_tag
  }
}

resource "aws_route_table" "rtb_public" {
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }

  tags = {
    "Environment" = var.environment_tag
  }
}

resource "aws_route_table_association" "rta_subnet_public" {
  subnet_id      = aws_subnet.subnet_public.id
  route_table_id = aws_route_table.rtb_public.id
}

resource "aws_security_group" "sg_22" {
  name = "sg_22"
  vpc_id = aws_vpc.vpc.id

  # SSH access from the VPC
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    "Environment" = var.environment_tag
  }
}
#resource "aws_key_pair" "ec2key" {
#  key_name = "publicKey"
#  public_key = file(var.public_key_path)
#}

resource "aws_instance" "microservice-app" {
  ami           = var.instance_ami
  instance_type = var.instance_type
  subnet_id = aws_subnet.subnet_public.id
  vpc_security_group_ids = [
    aws_security_group.sg_22.id,
    aws_security_group.web-80.id,
    aws_security_group.web-ping.id,
    aws_security_group.web-ssh.id,
    aws_security_group.web-sg.id
  ]
#  key_name = aws_key_pair.ec2key.key_name
  user_data     = file("init-script-centos.sh")
  key_name = "app-msa-dev"

  connection {
    host = self.public_ip
    type = "ssh"
    user = "ubuntu"
    private_key = file("./app-msa-dev.pem")
    agent = false
    timeout = "1m"
  }
  associate_public_ip_address = true
  tags = {
    "Environment" = var.environment_tag
  }
}

#####
#resource "aws_instance" "microservice-app" {
#  ami           = "ami-0dfcb1ef8550277af"
#  instance_type = "t2.large"
#  key_name = "app-msa-dev"
#  user_data     = file("init-script.sh")
#
#  connection {
#    host = self.public_ip
#    type = "ssh"
#    user = "ubuntu"
#    private_key = file("./app-msa-dev.pem")
#    agent = false
#    timeout = "1m"
#  }
#  security_groups = [
#    "app-tag-sg",
#    "app-tag-ssh-sg",
#    "app-tag-ping-sg",
#    "app-tag-http-sg"]
#  associate_public_ip_address = true
#
#  tags = {
#    "Environment" = var.environment_tag
#  }
#}

resource "aws_security_group" "web-sg" {
  name = "web-sg"
  description = "AWS security group for HTTP"
  vpc_id = aws_vpc.vpc.id
  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1" //"tcp" not valid
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    "Environment" = var.environment_tag
  }
}

resource "aws_security_group" "web-ssh" {
  name        = "web-ssh"
  description = "AWS security group for SSH access"
  vpc_id = aws_vpc.vpc.id
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    "Environment" = var.environment_tag
  }
}

resource "aws_security_group" "web-ping" {
  name        = "web-ping"
  description = "AWS security group for PING"
  vpc_id = aws_vpc.vpc.id
  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 8
    to_port     = 0
    protocol    = "icmp"
    description = "Allow ping from all"
  }
  tags = {
    "Environment" = var.environment_tag
  }
}

resource "aws_security_group" "web-80" {
  name = "web-80"
  description = "AWS security group for HTTP"
  vpc_id = aws_vpc.vpc.id
  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    description = "Allow http from all"
  }
  tags = {
    "Environment" = var.environment_tag
  }
}
